// Part 1
db.fruits.aggregate([
{$match: {onSale: true}},
{$match: {_id: null, fruitsOnSale: {$sum: 1}}},
{$project: {_id: 0}}]);


// PART 2
db.fruits.aggregate([
	{$group: {stock: {$gte: 20}}},
	{$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
	{$project: {_id: 0}}
]):


//PART 3
dib.fruits.aggregate([
	{$match: {onSAle: true}},
	{$group: {_id: "$supplier_id", avg_price: {$max: "$price"}}}
]);